```
  _    _           _       _              __  __          _ _ _       ______ _           __
 | |  | |         | |     | |            |  \/  |        (_) | |     |  ____(_)         / _|
 | |  | |_ __   __| | __ _| |_ ___ ______| \  / | ___ _____| | | __ _| |__   _ _ __ ___| |_ _____  __
 | |  | | '_ \ / _` |/ _` | __/ _ \______| |\/| |/ _ \_  / | | |/ _` |  __| | | '__/ _ \  _/ _ \ \/ /
 | |__| | |_) | (_| | (_| | ||  __/      | |  | | (_) / /| | | | (_| | |    | | | |  __/ || (_) >  <
  \____/| .__/ \__,_|\__,_|\__\___|      |_|  |_|\___/___|_|_|_|\__,_|_|    |_|_|  \___|_| \___/_/\_\
        | |
        |_|                                                                                                      
```

# Update-MozillaFirefox Wiki

[TOC]

## Screenshot

![Screenshot](https://bitbucket.org/auberginehill/update-mozilla-firefox/raw/b1412bb9fad2ab9af65b93905a72c4b250dcbf6b/Update-MozillaFirefox.png)




## Outputs 

:arrow_right: Displays Firefox related information in console.

  - Tries to update an outdated Firefox to its latest version, if an old Firefox installation is found, and if Update-MozillaFirefox is run in an elevated Powershell window. In addition to that...

  - At Step 7 the baseline Firefox version numbers are written to a file (`firefox_current_versions.json`) and also four additional auxillary JSON files are created, namely:

    **Firefox JSON Files** (at Step 7):

    | File                            | Path                                   |
    |---------------------------------|----------------------------------------|
    | `firefox_current_versions.json` | `%TEMP%\firefox_current_versions.json` |
    | `firefox_release_history.json`  | `%TEMP%\firefox_release_history.json`  |
    | `firefox_major_versions.json`   | `%TEMP%\firefox_major_versions.json`   |
    | `firefox_languages.json`        | `%TEMP%\firefox_languages.json`        |
    | `firefox_regions.json`          | `%TEMP%\firefox_regions.json`          |

    The `%TEMP%` location represents the current Windows temporary file folder. In PowerShell, for instance the command `$env:temp` displays the temp-folder path.

  - If the actual update procedure including the installation file downloading is initiated, a Firefox Install Configuration File (`firefox_configuration.ini`) is created with one active parameter (other parameters inside the file are commented out), and after Firefox has been updated, a web page displaying the latest version is opened in the default browser.

    **Install Configuration File** (at Step 14):

    | File                           | Path                                  |
    |--------------------------------|---------------------------------------|
    | `firefox_configuration.ini`    | `%TEMP%\firefox_configuration.ini`    |

      The `%TEMP%` location represents the current Windows temporary file folder. In PowerShell, for instance the command `$env:temp` displays the temp-folder path.

  - To see the actual values that are being written to the Install Configuration File (`firefox_configuration.ini`), please see the Step 14 in the [script](https://bitbucket.org/auberginehill/update-mozilla-firefox/src/master/Update-MozillaFirefox.ps1) itself, where the following value is written:

    | Value                      | Description               |
    |----------------------------|---------------------------|
    | `MaintenanceService=false` | The MozillaMaintenance service is used for silent updates and may be used for other maintenance related tasks. It is an optional component. This option can be used in Firefox 16 or later to skip installing the service. |

    For a comprehensive list of available settings and a more detailed description of the value above, please see the "[Installer:Command Line Arguments](https://wiki.mozilla.org/Installer:Command_Line_Arguments)" page.
  
  - To open these file locations in a Resource Manager Window, for instance a command...

    `Invoke-Item $env:temp` 

    ...may be used at the PowerShell prompt window `[PS>]`.




## Notes 

:warning: Requires either (a) PowerShell v3 or later or (b) .NET 3.5 or later for importing and converting JSON-files (at Step 8).

  - Requires a working Internet connection for downloading a list of the most recent Firefox version numbers and for downloading a complete Firefox installer from Mozilla (but the latter procedure is not initiated, if the system is deemed up-to-date).

  - For performing any actual updates with Update-MozillaFirefox, it's mandatory to run this script in an elevated PowerShell window (where PowerShell has been started with the 'run as an administrator' option). The elevated rights are needed for installing Firefox on top of the existing Firefox installation.

  - Update-MozillaFirefox is designed to update only one instance of Firefox. If more than one instances of Firefox are detected, the script will notify the user at Step 5, and furthermore, if old Firefox(es) are detected, the script will exit before downloading the installation file at Step 15.

  - Please note that the Firefox installation configuration file written at Step 14 disables the Mozilla Maintenance service so that the Mozilla Maintenance service will not be installed during the Firefox update. The values set with the Install Configuration File (`firefox_configuration.ini`) are altering the system files and seemingly are written somewhere deeper to the innards of Mozilla Firefox semi-permanently.

  - Please also notice that when run in an elevated PowerShell window and an old Firefox version is detected, Update-MozillaFirefox will automatically try to download files from the Internet without prompting the end-user beforehand or without asking any confirmations (at Step 16 and onwards) and at Step 17 closes a bunch of processes without any further notice.

  - Please note that the downloaded files are placed in a directory, which is specified with the `$path` variable (at line 42). The `$env:temp` variable points to the current temp folder. The default value of the `\$env:temp` variable is `C:\Users<username\>\AppData\Local\Temp` (i.e. each user account has their own separate temp folder at path `%USERPROFILE%\AppData\Local\Temp`). To see the current temp path, for instance a command...

    `[System.IO.Path]::GetTempPath()`

    ...may be used at the PowerShell prompt window `[PS\>]`.

  - To change the temp folder for instance to `C:\Temp`, please, for example, follow the instructions at [Temporary Files Folder - Change Location in Windows](http://www.eightforums.com/tutorials/23500-temporary-files-folder-change-location-windows.html), which in essence are something along the lines:
    1. Right click Computer icon and select Properties (or select Start → Control Panel → System. On Windows 10 this instance may also be found by right clicking Start and selecting Control Panel → System... or by pressing `[Win-key]` + X and selecting Control Panel → System). On the window with basic information about the computer...
    1. Click on Advanced system settings on the left panel and select Advanced tab on the "System Properties" pop-up window.
    1. Click on the button near the bottom labeled Environment Variables.
    1. In the topmost section, which lists the User variables, both TMP and TEMP may be seen. Each different login account is assigned its own temporary locations. These values can be changed by double clicking a value or by highlighting a value and selecting Edit. The specified path will be used by Windows and many other programs for temporary files. It's advisable to set the same value (a directory path) for both TMP and TEMP.
    1. Any running programs need to be restarted for the new values to take effect. In fact, probably Windows itself needs to be restarted for it to begin using the new values for its own temporary files.




## Examples

:book: To open this code in Windows PowerShell, for instance:

  1. `./Update-MozillaFirefox`

    Runs the script. Please notice to insert `./` or `.\` before the script name.

  1. `help ./Update-MozillaFirefox -Full`

    Displays the help file.

  1. `New-Item -ItemType File -Path C:\Temp\Update-MozillaFirefox.ps1`

    Creates an empty ps1-file to the `C:\Temp` directory. The `New-Item` cmdlet has an inherent `-NoClobber` mode built into it, so that the procedure will halt, if overwriting (replacing the contents) of an existing file is about to happen. Overwriting a file with the `New-Item` cmdlet requires using the `Force`. If the path name and/or the filename includes space characters, please enclose the whole `-Path` parameter value in quotation marks (single or double): `New-Item -ItemType File -Path "C:\Folder Name\Update-MozillaFirefox.ps1"`. For more information, please type "`help New-Item -Full`".

  1. `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine`

    This command is altering the Windows PowerShell rights to enable script execution in the default (`LocalMachine`) scope, and defines the conditions under which Windows PowerShell loads configuration files and runs scripts in general. In Windows Vista and later versions of Windows, for running commands that change the execution policy of the `LocalMachine` scope, Windows PowerShell has to be run with elevated rights (Run as Administrator). The default policy of the default (`LocalMachine`) scope is "`Restricted`", and a command "`Set-ExecutionPolicy Restricted`" will "undo" the changes made with the original example above (had the policy not been changed before...). Execution policies for the local computer (`LocalMachine`) and for the current user (`CurrentUser`) are stored in the registry (at for instance the `HKLM:\Software\Policies\Microsoft\Windows\PowerShell\ExecutionPolicy` key), and remain effective until they are changed again. The execution policy for a particular session (`Process`) is stored only in memory, and is discarded when the session is closed.

    |                | PowerShell Execution Policy Parameters                                         |
    |----------------|--------------------------------------------------------------------------------|
    | `Restricted`   | Does not load configuration files or run scripts, but permits individual commands. `Restricted` is the default execution policy. |
    | `AllSigned`    | Scripts can run. Requires that all scripts and configuration files be signed by a trusted publisher, including the scripts that have been written on the local computer. Risks running signed, but malicious, scripts. |
    | `RemoteSigned` | Requires a digital signature from a trusted publisher on scripts and configuration files that are downloaded from the Internet (including e-mail and instant messaging programs). Does not require digital signatures on scripts that have been written on the local computer. Permits running unsigned scripts that are downloaded from the Internet, if the scripts are unblocked by using the `Unblock-File` cmdlet. Risks running unsigned scripts from sources other than the Internet and signed, but malicious, scripts. |
    | `Unrestricted` | Loads all configuration files and runs all scripts. Warns the user before running scripts and configuration files that are downloaded from the Internet. Not only risks, but actually permits, eventually, runningany unsigned scripts from any source. Risks running malicious scripts. |
    | `Bypass`       | Nothing is blocked and there are no warnings or prompts. Not only risks, but actually permits running any unsigned scripts from any source. Risks running malicious scripts. |
    | `Undefined`    | Removes the currently assigned execution policy from the current scope. If the execution policy in all scopes is set to `Undefined`, the effective execution policy is `Restricted`, which is the default execution policy. This parameter will not alter or remove the ("master") execution policy that is set with a Group Policy setting. |
    | **Notes:**     | Please note that the Group Policy setting "`Turn on Script Execution`" overrides the execution policies set in Windows PowerShell in all scopes. To find this ("master") setting, please, for example, open the Local Group Policy Editor (`gpedit.msc`) and navigate to Computer Configuration → Administrative Templates → Windows Components → Windows PowerShell. The Local Group Policy Editor (`gpedit.msc`) is not available in any Home or Starter edition of Windows. |
    |                | For more information, please type "`Get-ExecutionPolicy -List`", "`help Set-ExecutionPolicy -Full`", "`help about_Execution_Policies`" or visit [Set-ExecutionPolicy](https://technet.microsoft.com/en-us/library/hh849812.aspx) or [About Execution Policies](http://go.microsoft.com/fwlink/?LinkID=135170). |




## Contributing

|        |                           |                                                                                                                        |
|:------:|---------------------------|------------------------------------------------------------------------------------------------------------------------|
| :herb: | **Bugs:**                 | Bugs can be reported by creating a new [issue](https://bitbucket.org/auberginehill/update-mozilla-firefox/issues/).               |
|        | **Feature Requests:**     | Feature request can be submitted by creating a new [issue](https://bitbucket.org/auberginehill/update-mozilla-firefox/issues/).   |
|        | **Editing Source Files:** | New features, fixes and other potential changes can be discussed in further detail by opening a [pull request](https://bitbucket.org/auberginehill/update-mozilla-firefox/pull-requests/). |

For further information, please see the [Contributing.md](https://bitbucket.org/auberginehill/update-mozilla-firefox/wiki/Contributing.md) page.




## Wiki Features

The wiki itself is actually a git repository, which means you can clone it, edit it locally/offline, add images or any other file type, and push it back to us. It will be live immediately.

```
$ git clone https://auberginehill@bitbucket.org/auberginehill/update-mozilla-firefox.git/wiki
```

Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.