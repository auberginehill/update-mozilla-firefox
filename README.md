
## Update-MozillaFirefox.ps1

|                  |                                                                                                            |
|------------------|------------------------------------------------------------------------------------------------------------|
| **OS:**          | Windows                                                                                                    |
| **Type:**        | A Windows PowerShell script                                                                                |
| **Language:**    | Windows PowerShell                                                                                         |
| **Description:** | Update-MozillaFirefox downloads a list of the most recent Firefox version numbers against which it compares the Firefox version numbers found on the system and displays, whether a Firefox update is needed or not. Update-MozillaFirefox detects the installed Firefoxes by querying the Windows registry for installed programs. The keys from `HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\` and `HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\` are read on 64-bit computers, and on the 32-bit computers only the latter path is accessed. At Step 7 Update-MozillaFirefox downloads and writes several Firefox-related files, namely "`firefox_current_versions.json`", "`firefox_release_history.json`", "`firefox_major_versions.json`", "`firefox_languages.json`" and "`firefox_regions.json`", which Update-MozillaFirefox uses as data sources. When run in a 'normal' PowerShell window, and all the detected Firefox versions seem to be up-to-date, Update-MozillaFirefox will just check that everything is OK and leave without further ceremony at Step 11. |
|                  | If Update-MozillaFirefox is run without elevated rights (but with a working Internet connection) in a machine with an old Firefox version, it will be shown that a Firefox update is needed, but Update-MozillaFirefox will exit at Step 12 before downloading any files. To perform an update with Update-MozillaFirefox, PowerShell has to be run in an elevated window (run as an administrator). If Update-MozillaFirefox is run in an elevated PowerShell window and no Firefox is detected, the script offers the option to install Firefox in the "**Admin Corner**" (step 11), where, in contrary to the main autonomous nature of Update-MozillaFirefox, an end-user input is required for selecting the bit-version and the language. In the "Admin Corner", one instance of either 32-bit or 64-bit version in one of the available languages is installable with Update-MozillaFirefox – the language selection covers over 30 languages. |
|                  | In the update procedure itself Update-MozillaFirefox downloads a full Firefox installer from Mozilla, which is equal to the type that is already installed on the system (same bit version and language). After writing the Install Configuration File (`firefox_configuration.ini` to `$path` at Step 14, where, for instance, the automatic Mozilla Maintenance service is disabled and the default shortcuts are enabled) and stopping several Firefox-related processes, Update-MozillaFirefox installs the downloaded Firefox on top of the existing Firefox installation, which triggers the in-built Firefox update procedure. |
|                  | For further information, please see the [Wiki](https://bitbucket.org/auberginehill/update-mozilla-firefox/wiki/).     |
| **Homepage:**    | https://bitbucket.org/auberginehill/update-mozilla-firefox                                                          |
|                  | Short URL: https://tinyurl.com/y2vbsjzc                                                                  |
| **Version:**     | 2.0                                                                                                        |
| **Downloads:**   | For instance [Update-MozillaFirefox.ps1](https://bitbucket.org/auberginehill/update-mozilla-firefox/src/master/Update-MozillaFirefox.ps1). Or [everything as a .zip-file](https://bitbucket.org/auberginehill/update-mozilla-firefox/downloads/). Or `git clone https://auberginehill@bitbucket.org/auberginehill/update-mozilla-firefox.git`. Or `git fetch && git checkout master`. |




### Screenshot

![Screenshot](https://bitbucket.org/auberginehill/update-mozilla-firefox/raw/b1412bb9fad2ab9af65b93905a72c4b250dcbf6b/Update-MozillaFirefox.png)




### www

|                        |                                                                              |                               |
|:----------------------:|------------------------------------------------------------------------------|-------------------------------|
| :globe_with_meridians: | [Script Homepage](https://github.com/auberginehill/update-mozilla-firefox)   |                               |
|                        | [PowerTips Monthly vol 8 January 2014](http://powershell.com/cs/PowerTips_Monthly_Volume_8.pdf#IDERA-1702_PS-PowerShellMonthlyTipsVol8-jan2014) (or one of the [archive.org versions](https://web.archive.org/web/20150110213108/http://powershell.com/cs/media/p/30542.aspx)) | Tobias Weltner |
|                        | [Test Internet connection](http://powershell.com/cs/blogs/tips/archive/2011/05/04/test-internet-connection.aspx) (or one of the [archive.org versions](https://web.archive.org/web/20110612212629/http://powershell.com/cs/blogs/tips/archive/2011/05/04/test-internet-connection.aspx)) | ps1 |
|                        | [Read Json Object in Powershell 2.0](http://stackoverflow.com/questions/17601528/read-json-object-in-powershell-2-0#17602226 ) | Goyuix |
|                        | [Creating a Menu](http://powershell.com/cs/forums/t/9685.aspx) (or one of the [archive.org versions](https://web.archive.org/web/20150910111758/http://powershell.com/cs/forums/t/9685.aspx)) |lamaar75 |
|                        | [How to run exe with/without elevated privileges from PowerShell](http://stackoverflow.com/questions/29266622/how-to-run-exe-with-without-elevated-privileges-from-powershell?rq=1) | alejandro5042 |
|                        | [What's the best way to determine the location of the current PowerShell script?](http://stackoverflow.com/questions/5466329/whats-the-best-way-to-determine-the-location-of-the-current-powershell-script?noredirect=1&lq=1) | JaredPar and Matthew Pirocchi |
|                        | [Powershell show elapsed time](http://stackoverflow.com/questions/10941756/powershell-show-elapsed-time) | Jeff |
|                        | [Adding a Simple Menu to a Windows PowerShell Script](https://technet.microsoft.com/en-us/library/ff730939.aspx) | Microsoft TechNet |
|                        | [Working with Hash Tables](https://technet.microsoft.com/en-us/library/ee692803.aspx) | Microsoft TechNet    |
|                        | [Determine installed PowerShell version](http://stackoverflow.com/questions/1825585/determine-installed-powershell-version?rq=1) | |
|                        | [ConvertFrom-Json](https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.utility/convertfrom-json) | |
|                        | [ConvertFrom-StringData](https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.utility/convertfrom-stringdata) | |
|                        | [PowerTip: Convert JSON File to PowerShell Object](https://blogs.technet.microsoft.com/heyscriptingguy/2014/04/23/powertip-convert-json-file-to-powershell-object/) | |
|                        | [PowerShell v2 Converts Dictionary to Array when returned from a function](http://stackoverflow.com/questions/32887583/powershell-v2-converts-dictionary-to-array-when-returned-from-a-function) | |
|                        | [Working with JSON and PowerShell](http://powershelldistrict.com/powershell-json/) |                         |
|                        | [Perfect Progress Bars for PowerShell](https://www.credera.com/blog/technology-insights/perfect-progress-bars-for-powershell/) | |
|                        | [Software Update](http://kb.mozillazine.org/Software_Update)                 | MozillaZine                   |
|                        | [Installer:Command Line Arguments](https://wiki.mozilla.org/Installer:Command_Line_Arguments) | Mozilla Wiki |
|                        | [Software Update:Checking For Updates](https://wiki.mozilla.org/Software_Update:Checking_For_Updates) | Mozilla Wiki |
|                        | [Mozilla Release Engineering](https://ftp.mozilla.org/pub/firefox/releases/latest/README.txt) |              |
|                        | [App.update.url](http://kb.mozillazine.org/App.update.url)                   | MozillaZine                   |
|                        | [http://www.figlet.org/](http://www.figlet.org/) and [ASCII Art Text Generator](http://www.network-science.de/ascii/) | ASCII Art |
|                        | [HTML To Markdown Converter](https://digitalconverter.azurewebsites.net/HTML-to-Markdown-converter) |        |
|                        | [Markdown Tables Generator](https://www.tablesgenerator.com/markdown_tables) |                               |
|                        | [HTML table syntax into Markdown](https://jmalarcon.github.io/markdowntables/) |                             |
|                        | [A HTML to Markdown converter written in JavaScript](https://domchristie.github.io/turndown/) | Dom Christie |
|                        | [Paste to Markdown](https://euangoddard.github.io/clipboard2markdown/)       |                               |
|                        | [Convert HTML or anything to Markdown](https://cloudconvert.com/html-to-md)  |                               |
